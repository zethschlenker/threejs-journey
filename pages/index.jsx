import Link from 'next/link';
import getSitemap from '../util/getSitemap';
import styles from '../styles/Home.module.css';

export default function Home({ pages }) {
  return (
    <div className={styles.home}>
      <ul className={styles.pageList}>
        {pages.map((page) => (
          <li key={page}>
            <Link href={page}>
              <a>{page}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export const getServerSideProps = async ({ res }) => {
  const staticPages = getSitemap('pages');

  return {
    props: {
      pages: staticPages,
    },
  };
};
