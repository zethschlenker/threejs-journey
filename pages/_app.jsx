import Link from 'next/link';
import '../styles/globals.css';
import '../styles/app.css';

const App = ({ Component, pageProps }) => {
  return (
    <div id="app">
      <header className="site-header">
        <Link href="/">
          <a>Home</a>
        </Link>
      </header>
      <main className="lesson">
        <Component {...pageProps} />
      </main>
    </div>
  );
};

export default App;
