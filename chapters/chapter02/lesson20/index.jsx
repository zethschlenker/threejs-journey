import { forwardRef, Suspense, useEffect, useRef, useState } from 'react';
import { Canvas, useFrame } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import useForwardedRef from '@bedrock-layout/use-forwarded-ref';
import * as THREE from 'three';
import gsap from 'gsap/all';

const Ball = forwardRef(({ position = [0, 0, 0], radius = 0.5, color = '#ff0000', ...props }, ref) => {
  const meshRef = useForwardedRef(ref);

  const [currentColor, setCurrentColor] = useState(color);
  const [active, setActive] = useState(false);

  const pointerEnter = (e) => {
    e.stopPropagation();
    setCurrentColor('blue');
  };
  const pointerLeave = (e) => {
    e.stopPropagation();
    setCurrentColor(color);
  };
  const pointerClick = (e) => {
    setActive(!active);
  };

  useFrame(() => {
    const endPosition = active ? 10 : position[1];
    gsap.to(meshRef?.current?.position, {
      y: endPosition,
      duration: 0.5,
    });
  });

  return (
    <mesh ref={meshRef} position={position} onPointerEnter={pointerEnter} onPointerLeave={pointerLeave} onClick={pointerClick} {...props}>
      <sphereGeometry args={[radius, 16, 16]} />
      <meshBasicMaterial color={currentColor} />
    </mesh>
  );
});
Ball.displayName = 'Ball';

const Scene = () => {
  const ball1Ref = useRef(null);
  const ball2Ref = useRef(null);
  const ball3Ref = useRef(null);

  useFrame(({ clock }) => {
    ball1Ref.current.position.z = Math.sin(clock.elapsedTime * 0.3) * 1.5
    ball2Ref.current.position.z = Math.sin(clock.elapsedTime * 0.8) * 1.5
    ball3Ref.current.position.z = Math.sin(clock.elapsedTime * 1.4) * 1.5
  });

  return (
    <Suspense fallback={null}>
      <OrbitControls minDistance={15} maxDistance={20} />
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <Ball ref={ball1Ref} position={[-2, 0, 0]} />
      <Ball ref={ball2Ref} position={[0, 0, 0]} />
      <Ball ref={ball3Ref} position={[2, 0, 0]} />
    </Suspense>
  );
};

const Lesson = () => {
  return (
    <Canvas dpr={[1, 2]}>
      <Scene />
    </Canvas>
  );
};

export default Lesson;
