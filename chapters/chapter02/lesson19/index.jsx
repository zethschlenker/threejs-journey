import { Suspense } from 'react';
import { Canvas, useFrame, useLoader } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as THREE from 'three';
import DatGui from 'react-dat-gui';
// import { TextureLoader } from 'three/src/loaders/TextureLoader';



const Galaxy = () => {
  const [data, setData] = useState({
    count: 100000,
    size: 0.01,
    radius: 2,
    branches: 5,
    spin: 1,
    randomness: 0.2,
    randomnessPower: 3,
    innerColor: new THREE.Color('red'),
    outerColor: new THREE.Color('blue'),
  });

  const [positions, colors] = useMemo(() => {
    const positions = new Float32Array(data.count * 3);
    const colors = new Float32Array(data.count * 3);

    for (let i = 0; i < data.count; i++) {
      const i3 = i * 3;
      const radius = Math.random() * data.radius;
      const branchAngle = (i % data.branches) / data.branches * Math.PI * 2;
      const spinAngle = radius * data.spin;

      const randomX = Math.pow(Math.random(), data.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * data.randomness * radius;
      const randomY = Math.pow(Math.random(), data.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * data.randomness * radius;
      const randomZ = Math.pow(Math.random(), data.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * data.randomness * radius;

      positions[i3] = Math.cos(branchAngle + spinAngle) * radius + randomX;
      positions[i3 + 1] = randomY;
      positions[i3 + 2] = Math.sin(branchAngle + spinAngle) * radius + randomZ;

      const mixedColor = data.innerColor.clone();
      mixedColor.lerp(data.outerColor, radius / data.radius);
      colors[i3] = mixedColor.r;
      colors[i3 + 1] = mixedColor.g;
      colors[i3 + 2] = mixedColor.b;
    }

    return [positions, colors];
  }, [data]);
  return (
    <mesh>
      <points position={[0, 0, 0]}>
        <bufferGeometry>
          <bufferAttribute attachObject={['attributes', 'position']} count={positions.length / 3} array={positions} itemSize={3} />
          <bufferAttribute attachObject={['attributes', 'color']} count={colors.length / 3} array={colors} itemSize={3} />
        </bufferGeometry>
        <pointsMaterial
          size={data.size}
          sizeAttenuation={true}
          depthWrite={false}
          // blending={THREE.AdditiveBlending}
          blending={THREE.NoBlending}
          vertexColors={true}
        />
      </points>
    </mesh>
  );
};

const Scene = () => {
  return (
    <Suspense fallback={null}>
      <OrbitControls />
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <Galaxy />
    </Suspense>
  );
};

const Lesson = () => {
  return (
    <Canvas dpr={[1, 2]}>
      <Scene />
    </Canvas>
  );
};

export default Lesson;
