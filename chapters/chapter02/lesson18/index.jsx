import { Suspense } from 'react';
import { Canvas, useFrame, useLoader } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as THREE from 'three';
// import { TextureLoader } from 'three/src/loaders/TextureLoader';

const Points = ({ particleCount = 500 }) => {
  const particlesRef = useRef();
  const bufferRef = useRef();

  const particleTexture = useLoader(THREE.TextureLoader, '/chapter02/lesson18/textures/particles/2.png');
  const [coords, colors] = useMemo(() => {
    const coords = new Float32Array(particleCount * 3);
    const colors = new Float32Array(particleCount * 3);
    for (let i = 0; i < particleCount * 3; i++) {
      coords[i] = (Math.random() - 0.5) * 10;
      colors[i] = Math.random();
    }

    return [coords, colors];
  }, [particleCount]);

  useFrame(({ clock }) => {
    // Rotate entire group.
    particlesRef.current.rotation.y = clock.elapsedTime * 0.2;

    // Modify each of the buffer attributes.
    for (let i = 0; i < particleCount; i++) {
      const i3 = i * 3;
      const x = bufferRef.current.attributes.position.array[i3];
      bufferRef.current.attributes.position.array[i3 + 1] = Math.sin(clock.elapsedTime + x);
    }
    
    bufferRef.current.attributes.position.needsUpdate = true;
  });
  

  return (
    <>
      <mesh>
        <points ref={particlesRef} position={[0, 0, 0]}>
          <bufferGeometry ref={bufferRef}>
            <bufferAttribute attachObject={['attributes', 'position']} count={coords.length / 3} array={coords} itemSize={3} />
            <bufferAttribute attachObject={['attributes', 'color']} count={colors.length / 3} array={colors} itemSize={3} />
            {/* <instancedBufferAttribute attachObject={['attributes', 'color']} args={[colors, 3]} /> */}
          </bufferGeometry>
          <pointsMaterial
            size={0.1}
            sizeAttenuation={true}
            // color="red"
            alphaMap={particleTexture}
            vertexColors={true}
            transparent={true}
            blending={THREE.AdditiveBlending}
            // alphaTest={0.001}
            // depthTest={false}
            depthWrite={false}
          />
        </points>
      </mesh>
      {/* <mesh>
        <boxBufferGeometry />
        <meshBasicMaterial color="blue" />
      </mesh> */}
    </>
  );
};

const Scene = () => {
  return (
    <Suspense fallback={null}>
      <OrbitControls />
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <Points particleCount={20000} />
    </Suspense>
  );
};

const Lesson = () => {
  return (
    <Canvas dpr={[1, 2]}>
      <Scene />
    </Canvas>
  );
};

export default Lesson;
