module.exports = {
  apps: [
    {
      name: 'threejs-journey.zeth.dev',
      script: 'npm',
      args: 'start',
    },
  ],
};
