import fs from 'fs';
import path from 'path';

const getSitemap = (dirPath, arrayOfFiles) => {
  const files = fs.readdirSync(dirPath);

  arrayOfFiles = arrayOfFiles || [];

  files.forEach(function(file) {
    if (fs.statSync(dirPath + '/' + file).isDirectory()) {
      arrayOfFiles = getSitemap(dirPath + '/' + file, arrayOfFiles);
    } else {
      if (![
        'index.jsx',
        '_app.jsx',
        '_document.jsx',
      ].includes(file)) {
        arrayOfFiles.push(path.join(dirPath, '/', file).replaceAll('pages', '').replaceAll('.jsx', ''));
      }
    }
  });

  return arrayOfFiles.reverse();

  // return [];
};

export default getSitemap;