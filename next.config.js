const withPlugins = require('next-compose-plugins');
const withTM = require('next-transpile-modules')(['gsap']);

const nextConfig = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  experimental: {
    esmExternals: true,
  },
  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
  },
};

module.exports = withPlugins([
  [withTM],
], nextConfig);
